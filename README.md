# Overview

This repo contains the data, analysis, and survey used in the paper "[Users Can Deduce Sensitive Locations Protected by Privacy Zones on Fitness Tracking Apps](https://jaronm.ink/assets/pdf/papers/users-can-deduce_chi22.pdf)".
Please direct any questions to jaronmm2@illinois.edu.


## Materials
- **data**: This folder contains participants anonymized results for participants self-reported survey (survey_responses_full.csv) and inference task (survey_map_results_full.csv) as well as participant treatments (condition_lists.csv) and their corresponding participant assignments (participant_completions_full.csv).
- **analysis**: This folder contains three R scripts that produce the reported results in the pre-task (RQ1-pre-task-survey.Rmd), task (RQ2-inference_task.Rmd), and post-task (RQ3-post-task-survey.Rmd) survey.
- **survey**: This folder contains screenshots of each page (surveyXX.png) presented to participants. The survey materials may also be found in Appendix A of the paper.
